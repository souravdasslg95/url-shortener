const express = require('express');
export const app = express();
import session from "express-session";

const sessionMiddleware = session({
    secret: "changeit",
    saveUninitialized: true,
    resave: true
});

app.use(sessionMiddleware);
app.use(express.json({}));
app.use(express.urlencoded({ extended: true }));

export {
    sessionMiddleware
}