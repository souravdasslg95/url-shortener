import { JsonDB, Config } from 'node-json-db';


export interface URLInput {
    id: string
    originalURL: string,
}
// Interfaces for Data 
export interface Data {
    url: URL[]
}

export const db = new JsonDB(new Config('db', true, true, '/'));
export enum Collections {
    URLCollection = '/url'
}
