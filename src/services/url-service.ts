import { generateURLSafeId, isValidURL } from "../util";
import { container, injectable } from "tsyringe";
import { URLRepository } from "../repositories";
import { SocketMessage } from "../socket";
import { Request } from "express";

@injectable()
export class URLService {
  socketMessage = container.resolve(SocketMessage);
  async minifyURL(url: string, context: Request) {
    if (!isValidURL(url)) throw new Error("Invalid URL");
    const id = generateURLSafeId();
    const shortURL = `${process.env.SHORT_URL_HOST}/${id}`;
    const urlRepository = new URLRepository();
    await urlRepository.saveShortedURL({ id, originalURL: url });
    this.socketMessage.push({
      topic: "url",
      message: shortURL,
      sessionID: context.sessionID,
    });
  }
  async getURLById(id: string) {
    const urlRepository = new URLRepository();
    return urlRepository.getURLById(id);
  }
}
