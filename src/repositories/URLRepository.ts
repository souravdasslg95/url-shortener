import { injectable } from "tsyringe";
import { db, Collections, URLInput } from "../app/db";
import { SaveShortedURL } from "./interfaces";


@injectable()
export class URLRepository {
    async saveShortedURL({originalURL,id}: SaveShortedURL) {
        const data: URLInput = {
            id, originalURL
        }
        db.push(`${Collections.URLCollection}/${id}`,data)
    }

    async getURLById(id: string): Promise<URLInput | null> {
        try {
            const data = await db.getData(`${Collections.URLCollection}/${id}`)
            return data
        }
        catch (e) { 
            return null
        }
    }
}