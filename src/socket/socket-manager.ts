import { Socket, Server } from "socket.io";
import { container, singleton } from "tsyringe";
import { sessionMiddleware } from "../app";
import { wrap } from "./socket-middleware";
export const io = new Server();

// Use the same session middleware as express
io.use(wrap(sessionMiddleware));

// SocketManager is responsible for managing the socket connections
@singleton()
export class SocketManager {
  sockets: Map<string, Socket> = new Map();
  constructor() {
    console.log("MightyByteSocket");
  }
  addSocketClient(socket: Socket) {
    // @ts-ignore
    this.sockets.set(socket.request.clientID, socket);
  }
  removeSocketClient(socket: Socket) {
    // @ts-ignore
    this.sockets.delete(socket.request.clientID);
  }
  get(sessionID: string) {
    return this.sockets.get(sessionID);
  }
}

io.on("connection", (socket) => {
  const socketManager = container.resolve(SocketManager);
  socketManager.addSocketClient(socket);
  socket.on("disconnect", () => socketManager.removeSocketClient(socket));
});
