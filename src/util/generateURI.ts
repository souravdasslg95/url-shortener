export function generateURI(version: string) {
    return `/api/${version}`
}