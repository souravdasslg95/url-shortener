import { Request, Response } from 'express';
import { app } from '../../../app';
import { URLService } from '../../../services';
async function resolveURL(req: Request, res: Response) {
    const { id } = req.params;
    const urlService = new URLService();
    const url = await urlService.getURLById(id);

    if(!url) {
        return res.status(404).send({success: false, message: 'URL not found'})
    }

    res.redirect(301,url.originalURL)
}

app.get('/:id', resolveURL)