import { app } from "../../../app";
import { Request, Response } from "express";
import { generateURI } from "../../../util";
import { Versions } from "../../versions";
import {URLService} from '../../../services/'

async function shortenURL(req: Request, res: Response) {
    const urlService = new URLService()
    const url = req.body.url
    try {
      await urlService.minifyURL(url, req);
      res.json({ success: true, sessionId: req.sessionID });
    } catch (e) {
      res.status(500).json({ success: false, error: e.message });
    }

}

app.post(`${generateURI(Versions.V1)}/url`, shortenURL)