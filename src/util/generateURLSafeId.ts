import { urlAlphabet, customAlphabet } from 'nanoid'
export function generateURLSafeId() {
    return customAlphabet(urlAlphabet, 5)()
}