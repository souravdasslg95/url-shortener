import { container, singleton } from "tsyringe";
import { io, SocketManager } from "./socket-manager";
import { nanoid } from "nanoid";
import { SocketMessageQueue } from "./message-queue";

interface Message {
  sessionID: string;
  topic: string;
  message: string;
}

// SocketMessage is only responsible for sending messages to the client
// It handles the socket operations
@singleton()
export class SocketMessage {
  messageQueue = container.resolve(SocketMessageQueue);
  socketManager = container.resolve(SocketManager);

  push({ topic, message, sessionID }: Message) {
    const socket = this.socketManager.get(sessionID);
    const messageId = nanoid(5);
    this.messageQueue.addMessage({
      socketId: sessionID,
      message,
      messageId,
      topic,
    });
    if (socket) {
      socket.emit(topic, message, messageId, (ackMessageId: string) => {
        this.messageQueue.removeMessage(ackMessageId);
        console.debug("acknowledged message", ackMessageId);
      });
    }
  }
  retryPendingMessages(sessionID: string) {
    const messages = this.messageQueue.getMessagesOfSocket(sessionID);
    const socket = this.socketManager.get(sessionID);
    if (socket) {
      messages.forEach(({ message, messageId, topic }) => {
        socket.emit(topic, message, messageId, (ackMessageId: string) => {
          this.messageQueue.removeMessage(ackMessageId);
          console.debug("acknowledged pending message", ackMessageId);
        });
      });
    }
  }
}

io.on("connection", (socket) => {
  const socketMessage = container.resolve(SocketMessage);
  console.log("initiating retry pending messages", socket.request.sessionID);
  socketMessage.retryPendingMessages(socket.request.sessionID);
});
