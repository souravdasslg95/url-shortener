import { Socket } from "socket.io";
import { parseCookieString } from "../util";

// This middleware is used to extract the sessionID from the cookie and inject in socket.request
export const wrap =
  (middleware: Function) => (socket: Socket, next: Function) => {
    const cookies = parseCookieString(socket.request.headers.cookie);
    // @ts-ignore
    socket.request.clientID = cookies["connect.sid"].split(".")[0].slice(2);
    middleware(socket.request, {}, next);
  };
