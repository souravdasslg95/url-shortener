import "reflect-metadata"
import { static as staticMiddleware } from 'express'
import { app } from './app'
import {io} from './socket'
const http = require('http');
const server = http.createServer(app);
io.attach(server)

// import controllers
import './controllers/'
import './socket/'
import './config/env'


app.use('/', staticMiddleware(`${__dirname}/public`))

server.listen(process.env.PORT, () => {
  console.log(`Server is up`);
  console.log(`http://localhost:${process.env.PORT}`);
});
