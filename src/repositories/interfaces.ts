export interface SaveShortedURL {
    id: string
    originalURL: string
}