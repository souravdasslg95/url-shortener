import { Socket } from "socket.io";

interface ExtendedSocket extends Socket {
    sessionID: string;
  }