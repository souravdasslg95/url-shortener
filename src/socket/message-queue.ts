import { singleton } from "tsyringe";
import { filter, remove } from "lodash";

interface Message {
  messageId: string;
  socketId: string;
  message: any;
  topic: string;
}

// SocketMessageQueue is responsible for storing messages in memory. It abstracts away the storage mechanism
@singleton()
export class SocketMessageQueue {
  private messageQueue = new Array<Message>();

  addMessage({ socketId, message, messageId, topic }: Message) {
    this.messageQueue.push({ messageId, socketId, message, topic });
    console.log("pushing message", this.messageQueue.length);
    return messageId;
  }

  removeMessage(messageId: string) {
    return remove(this.messageQueue, (item) => {
      return item.messageId === messageId;
    });
  }
  getMessagesOfSocket(socketId: string) {
    return filter(this.messageQueue, { socketId });
  }

  getMessageQueueStats() {
    return {
      length: this.messageQueue.length,
    };
  }
}
