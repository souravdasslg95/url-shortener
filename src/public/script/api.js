function send() {
  const formData = new URLSearchParams();
  const headers = new Headers();
  headers.append("Content-Type", "application/x-www-form-urlencoded");
  const url = document.querySelector('input[name="url"]').value;
  formData.append('url', url);
  console.log(url);
  fetch("/api/v1/url", {
    method: "POST",
    headers: headers,
    body: formData,
  })
    .then((response) => {
      response.json().then((data) => {
        if (data.error) {
          alert(data.error);
          return;
        }
      });
    })
    .catch((error) => {
      alert("Invalid URL");
    });
  }