const socket = io("localhost:3000");

socket.on("connect", () => {
  console.log("Connected with server");
  const element = document.getElementById("socket-status");
  element.innerText = "Connected";
});

socket.on("url", (data, messageId, ack) => {
  const element = document.getElementById("shortened-url");
  console.log(element);
  element.href = data;
  element.innerText = data;
  ack(messageId);
});

socket.on("disconnect", () => {
  const element = document.getElementById("socket-status");
  element.innerText = "Disconnected";
  console.log("Disconnected from server");
});

function disconnect() {
  socket.disconnect();
}

function reconnect() {
  socket.connect();
}
